#include <iostream>
void inicializar_tablero(char tablero[3][3],  int dim)
{

    for (int i=0; i<dim ; i++)
    {

        for(int j=0; j<dim; j++)
        {


            tablero[i][j]=' ';

        }


    }
}
void insertar_tablero(char tablero[3][3],int pos,int jugador)
{

// jugador 1 es 1 en otro caso jugador 2 para simbolo

    switch(pos)
    {

    case 1:
        if(jugador==1)
        {
            tablero[2][0]='x';
        }
        else
        {

            tablero[2][0]='o';
        }
        break;

    case 2:
        if(jugador==1)
        {
            tablero[2][1]='x';
        }
        else
        {

            tablero[2][1]='o';
        }
        break;

    case 3:
        if(jugador==1)
        {
            tablero[2][2]='x';
        }
        else
        {

            tablero[2][2]='o';
        }
        break;

    case 4:
        if(jugador==1)
        {
            tablero[1][0]='x';
        }
        else
        {

            tablero[1][0]='o';
        }
        break;




    case 5:
        if(jugador==1)
        {
            tablero[1][1]='x';
        }
        else
        {

            tablero[1][1]='o';
        }
        break;


    case 6:
        if(jugador==1)
        {
            tablero[1][2]='x';
        }
        else
        {

            tablero[1][2]='o';
        }
        break;

    case 7:
        if(jugador==1)
        {
            tablero[0][0]='x';
        }
        else
        {

            tablero[0][0]='o';
        }
        break;


    case 8:
        if(jugador==1)
        {
            tablero[0][1]='x';
        }
        else
        {

            tablero[0][1]='o';
        }
        break;
    case 9:
        if(jugador==1)
        {
            tablero[0][2]='x';
        }
        else
        {

            tablero[0][2]='o';
        }
        break;


    }
}

void imprimir_tablero(char tablero[3][3], int dim)
{

    for (int i=0; i<dim ; i++)
    {

        for(int j=0; j<dim; j++)
        {


            std::cout<<tablero[i][j]<<"\t";

        }


        std::cout <<" "<<  std::endl;


    }




}


char get_tablero(char tablero[3][3],int pos)
{


    switch(pos)
    {

    case 1:

        return tablero[2][0];


    case 2:

        return tablero[2][1];



    case 3:

        return tablero[2][2];


    case 4:

        return tablero[1][0];





    case 5:

        return  tablero[1][1];



    case 6:

        return tablero[1][2];


    case 7:

        return tablero[0][0];




    case 8:

        return tablero[0][1];



    case 9:

        return  tablero[0][2];

    default:
        return -1;



    }


}


bool chequear_ganador(char tablero[3][3], int dim, char simbol)
{
    int contador=0;
    bool win=false;


    for (int i=0; i<dim ; i++)
    {
        for(int j=0; j<dim ; j++ )
        {

            if(tablero[i][j]==simbol)
            {

                contador++;

            }




        }

          if(contador==3)
            {

                win=true;
                return win;
            }
            else
            {
                contador=0;


            }


    }

    for (int i=0; i<dim ; i++)
    {
        for(int j=0; j<dim ; j++ )
        {

            if(tablero[j][i]==simbol)
            {

                contador++;

            }




        }

          if(contador==3)
            {

                win=true;
                return win;
            }
            else
            {
                contador=0;


            }


    }

    for (int i=0; i<dim ; i++)
    {


        if(tablero[i][i]==simbol)
        {

            contador++;

        }



    }

     if(contador==3)
        {

            win=true;
            return win;
        }
        else
        {
            contador=0;


        }

        for (int i=0; i<dim  ; i++)
    {


        if(tablero[i][dim-1-i]==simbol)
        {

            contador++;


        }



    }

      if(contador==3)
        {

            win=true;
            return win;
        }
        else
        {
            contador=0;


        }


return win;

}



int main()
{
    std::string nombre1, nombre2;
    char tablero[3][3];
    std::cout << "Ingrese su nombre (Jugador 1)" << std::endl;
    std::getline(std::cin,nombre1);
    std::cout << "Ingrese su nombre (Jugador 2)" << std::endl;
    std::getline(std::cin,nombre2);
    bool ended1=false;
    bool ended2=false;
    char control='1';
    int pos;
    inicializar_tablero(tablero,3);
    while(!ended1 && !ended2)
    {
        while(control!=' ')
        {
            std::cout <<nombre1<< " Ingrese una posicion 1 a 9 " <<  std::endl;
            std::cin>>pos;
            control=get_tablero(tablero,pos);
            if(control==' ')
            {
                insertar_tablero(tablero,pos,1);
            }
            else
            {

                std::cout <<"Posicion Ocupada: Intente nuevamente" <<  std::endl;
            }

        }
        system("cls");
        imprimir_tablero(tablero,3);
        ended1=chequear_ganador(tablero,3,'x');
        control='1';

        while(control!=' ')
        {
            std::cout <<nombre2<< " Ingrese una posicion 1 a 9 " <<  std::endl;
            std::cin>>pos;
            control=get_tablero(tablero,pos);
            if(control==' ')
            {
                insertar_tablero(tablero,pos,2);
            }
            else
            {

                std::cout <<"Posicion Ocupada: Intente nuevamente" <<  std::endl;
            }

        }
        system("cls");
        imprimir_tablero(tablero,3);
        ended2=chequear_ganador(tablero,3,'o');
        control='1';

    }

    if(ended1 && ended2){

        std::cout<<"Empate"<<std::endl;
    }else{

    if(ended1){
       std::cout<<"Jugador 1 ganador"<<std::endl;

    }

     if(ended2){
       std::cout<<"Jugador 2 ganador"<<std::endl;

    }

    if(!ended1 && !ended2){

        std::cout<<"Ninguno gano"<<std::endl;
    }


    }

    return 0;
}
