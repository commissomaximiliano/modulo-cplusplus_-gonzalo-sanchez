#include "source.cpp"
#define VIEWS 3


int main()
{

    char control='y';
    int view;
    ViewData1 one_view;
    ViewData2 two_view;
    ViewData3 three_view;
    float temp,hum,press;
    std::string message[VIEWS];


    std::cout << "enter message view 1" << std::endl;
    std::cin>>message[0];
    std::cout << "enter message view 2" << std::endl;
    std::cin>>message[1];
    std::cout << "enter message view 3" << std::endl;
    std::cin>>message[2];


    while(control=='y')
    {
        std::cout << "enter temperature" << std::endl;
        std::cin>>temp;
        std::cout << "enter humidity" << std::endl;
        std::cin>>hum;
        std::cout << "enter press" << std::endl;
        std::cin>>press;


        one_view.add_temp(temp);
        one_view.add_press(press);
        one_view.add_hum(hum);
        two_view.add_temp(temp);
        two_view.add_press(press);
        two_view.add_hum(hum);
        three_view.add_hum(hum);
        three_view.add_temp(temp);
        three_view.add_press(press);

        std::cout << "Continue? y/n" << std::endl;
        std::cin>>control;
    }

    one_view.printData(message[0]);
    two_view.printData(message[1]);
    three_view.printData(message[2]);


    return 0;
}
