#include <iostream>
#include <list>
#include <string>

class Data
{

private:
    std::list<float> temp_list;
    std::list<float> hum_list;
    std::list<float> press_list;

public:

    void set_temps(std::list<float> temp_list)
    {

        this->temp_list=temp_list;
    }

    void set_hums(std::list<float> hum_list)
    {

        this->hum_list=hum_list;
    }

    void set_press(std::list<float> press_list)
    {

        this->press_list=press_list;
    }

    std::list<float> get_temps()
    {

        return temp_list;
    }

    std::list<float> get_hums()
    {

        return hum_list;
    }

    std::list<float> get_press()
    {

        return press_list;
    }

    void add_temp(float t)
    {

        temp_list.push_back(t);
    }


    void add_hum(float h)
    {

        hum_list.push_back(h);
    }


    void add_press(float p)
    {

        press_list.push_back(p);
    }


    virtual void printData(std::string message) {};

};


class ViewData1: public Data
{


public:
    void printData(std::string message) override
    {


        std::cout<<message<<"  Temperature: "<<get_temps().back()<<"C"<<std::endl;
        std::cout<<message<<"  Humidity: "<<get_hums().back()<<"%"<<std::endl;
        std::cout<<message<<"  Press: "<<get_press().back()<<"Pa"<<std::endl;

    }

};

class ViewData2: public Data
{

public:
    void printData(std::string message) override
    {
        float temp_min=0,temp_max=0,temp_avg=0;
        float press_min=0,press_max=0,press_avg=0;
        float hum_min=0,hum_max=0,hum_avg=0;



        temp_max=get_temps().front();
        temp_min=get_temps().front();
        hum_min=get_hums().front();
        hum_max=get_hums().front();
        press_min=get_press().front();
        press_max=get_press().front();
        for( auto temp:get_temps())
        {

            if(temp<temp_min)
                temp_min=temp;
            if(temp>temp_max)
                temp_max=temp;


            temp_avg+=temp;


        }

        for(auto hum:get_hums())
        {


            if(hum<hum_min)
                hum_min=hum;
            if(hum>hum_max)
                hum_max=hum;

            hum_avg+=hum;


        }


        for (auto press:get_press())
        {



            if(press<press_min)
                press_min=press;
            if(press>press_max)
                press_max=press;


            press_avg+=press;

        }


        std::cout<<message<<"  Temperature: MIN/AVG/MAX in C "<<temp_min<<"/"<<temp_avg/get_temps().size()<<"/"<<temp_max<<std::endl;
        std::cout<<message<<"  Humidity: MIN/AVG/MAX in % "<<hum_min<<"/"<<hum_avg/get_hums().size()<<"/"<<hum_max<<std::endl;
        std::cout<<message<<"  Press: MIN/AVG/MAX in Pa "<<press_min<<"/"<<press_avg/get_press().size()<<"/"<<press_max<<std::endl;

    }






};


class ViewData3: public Data
{



public:
    void printData(std::string message) override
    {

        float temp=get_temps().back();
        float hum=get_hums().back();

        if(temp > -25 && temp <=10) std::cout<<message<<" careful not go out today \n";
        if(temp > 10 && temp <=20) std::cout<<message<<" use winter clothes \n";
        if(temp > 20 && temp <=30) std::cout<<message<<" Good Day today \n";
        if(temp > 30 && temp <=44) std::cout<<message<<" dehydration danger \n";

        if(hum > 0 && hum <=25) std::cout<<message<<" dryness today \n";
        if(hum > 25 && hum <=50) std::cout<<message<<" nice time today\n";
        if(hum > 50 && hum <=75) std::cout<<message<<" chance of rain \n";
        if(hum > 75 && hum <=100) std::cout<<message<<" chance of heavy rain and storms \n";




    }


};
