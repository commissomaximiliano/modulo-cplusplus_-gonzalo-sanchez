#include <iostream>
#include "point.h"
using namespace std;

int main()
{
    point nuevo(2.5,3.5);
    point nuevo2;
    nuevo2.set_X(12.1);
    nuevo2.set_Y(1.1);
    std::cout << "los puntos son : P1: "<<std::endl;
    nuevo.point_print();
    std::cout << "los puntos son : P2: "<<std::endl;
    nuevo2.point_print();
    std::cout << "Sus distancias al origen : D_P1: " <<nuevo.distance()<<" D_P2: "<<nuevo2.distance()<<std::endl;
    return 0;
}
