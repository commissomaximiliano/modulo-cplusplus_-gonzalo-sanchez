#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

class point{

private:
    float x;
    float y;

public:
    point();
    point(float x , float y);
    float distance();
    float get_X();
    void set_X(float);
    void set_Y(float);
    float get_Y();
    void point_print();




};


#endif // POINT_H_INCLUDED
