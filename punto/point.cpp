#include "point.h"
#include "math.h"
#include <iostream>
point::point(){}


point::point(float x , float y){

this->x=x;
this->y=y;
}

float point::distance(){

return sqrt(pow(this->x,2)+pow(this->y,2));

}

float point::get_X(){

return this->x;
}

float point::get_Y(){

return this->y;
}

void point::set_X(float x){
this->x=x;
}

void point::set_Y(float y){
this->y=y;
}

void point::point_print(){
std::cout << "x : " <<this->x<< " y : "<<this->y<< std::endl;

}
