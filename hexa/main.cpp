#include <iostream>
#include <string>
#include <math.h>

int get_num(char num)
{

    switch(num)
    {

    case '1':
        return 1;
    case '2':
        return 2;
    case '3':
        return 3;
    case '4':
        return 4;
    case '5':
        return 5;
    case '6':
        return 6;
    case '7':
        return 7;
    case '8':
        return 8;
    case '9':
        return 9;
    case 'a':
    case 'A':
        return 10;
    case 'b':
    case 'B':
        return 11;
    case 'c':
    case 'C':
        return 12;
    case 'd':
    case 'D':
        return 13;
    case 'e':
    case 'E':
        return 14;
    case 'f':
    case 'F':
        return 15;
    default:
        return 0;

    }



}


int conver_decimal(std::string hexa)
{

    std::string hexa_corto=hexa.substr(2,hexa.size());
    int value=0;
    int acum=0;
    int longitud=(int)hexa_corto.size();
    for(int i=0; i<longitud; i++)
    {

        value=get_num(hexa_corto[longitud-1-i]);

        acum+=value*(int)pow(16,i);


    }
    return acum;
}

int main()
{
    std:: string hexa="0x7e5";
    std::cout << conver_decimal(hexa)<< std::endl;
    return 0;
}
